Folder for sort algorithms

bubblesort.py		is my version of the bubble sort algorithm written in python 3
insertionsort.py	is my version of the insertion sort algorithm written in python 3
rangen.py		is used to generate a .txt file with a stream of random numbers used to test the sort algorithms
selectionsort.py	is my version of the selection sort sort algorithm written in python 3
shellsort.py		is my version of the shell sort algorithm written in python 3